﻿using HogsOfWarObjectEditor2.Objects;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Controls.Ribbon;

namespace HogsOfWarObjectEditor2
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        private static ObjectEditor ReadInstance = null;


       

        public MainWindow() : base()
        {

            InitializeComponent();

   

        }



        public void OpenFile()
        {

            OpenFileDialog Dialog = new OpenFileDialog();
            Dialog.Filter = " HogsOfWar ObjectFile |*.POG";
        
          
            if (Dialog.ShowDialog().Value)
            {
                using (MemoryStream Stream = new MemoryStream(File.ReadAllBytes(Dialog.FileName)))
                {


                    NavigateToFrame(new ProgressBar());

                    using (StreamReader Reader = new StreamReader(Stream))
                    {
                        if (!Reader.Read(out ushort ObjectsCount))
                        {
                            MessageBox.Show("Invalid Read ObjectCount");
                        }

                        if(ObjectsCount > 10)
                        {
                            //NextButton.Visibility = Visibility.Visible;
                        }
                
                        List<byte> groups = new List<byte>();
                        for (int i = 0; i < ObjectsCount; i++)
                        {
                            MapObject Row = new MapObject();

                            if (!Row.Read(Reader))
                            {
                                MessageBox.Show("Failed to Read MapObject from file..");
                                break;
                            }

                       
                            if (!groups.Contains(Row.GroupValue))
                            {
                                groups.Add(Row.GroupValue);

                            }
          

                            ReadInstance.AddObject(Row);
             




                        }
                    }
                }

            }
            else
            {
                System.Environment.Exit(0);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ReadInstance != null)
            {
                ReadInstance.ResetFunctions();
               // MapObjectList.Clear();
                OpenFile();
            }
            else
            {
                ReadInstance = new ObjectEditor();
                OpenFile();
            }
        }

        private void NavigateEditContent(Page NewContent)
        {

        }





        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            SaveFileDialog Dialog = new SaveFileDialog();

            Dialog.Filter = " HogsOfWar ObjectFile |*.POG";



            if (Dialog.ShowDialog() == true)
            {
              

                using (FileStream NeFole = new FileStream(Dialog.FileName, FileMode.Create))
                {
                    using (StreamWriter Writer = new StreamWriter(NeFole))
                    {
                        ReadInstance.WriteFullObjectData(Writer);

                    }
                }
            }


        }


        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            /*
            if(!MapObjectList.NextPage())
            {
                //NextButton.Visibility = Visibility.Hidden;
            }
            else
            {
               // BackButton.Visibility = Visibility.Visible;
            }*/
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            /*
            if (!MapObjectList.BackPage())
            {
              //  BackButton.Visibility = Visibility.Hidden;
            }
            else
            {
            //    NextButton.Visibility = Visibility.Visible;
            }*/
        }


        private void Ribbon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
          
         
     
            switch((sender as Ribbon)?.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    break;
            }

          //  MessageBox.Show(m.SelectedIndex.ToString());
        }

        public void NavigateToFrame(object Ob)
        {
            switch(Ob)
            {
                case ProgressBar prog when Ob is ProgressBar:
                    EditControl.Navigate(prog);
                    break;
            }
        }

    }
}
