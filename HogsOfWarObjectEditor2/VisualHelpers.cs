﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;


    public static class VisualHelpers
    {
        //https://social.msdn.microsoft.com/Forums/vstudio/en-US/b68381b1-8707-41e4-822e-9a9334becdca/how-to-get-access-to-usercontrol-in-listbox-using-datatemplate?forum=wpf
        //thanks

        public static Visual GetDescendantByType(Visual element, Type type)

        {

            if (element == null) return null;



            if (element.GetType() == type) return element;



            Visual foundElement = null;

            if (element is FrameworkElement)

                (element as FrameworkElement).ApplyTemplate();



            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)

            {

                Visual visual = VisualTreeHelper.GetChild(element, i) as Visual;

                foundElement = GetDescendantByType(visual, type);

                if (foundElement != null)

                    break;

            }

            return foundElement;

        }
    }

