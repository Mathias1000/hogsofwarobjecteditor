﻿using HogsOfWarObjectEditor2.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HogsOfWarObjectEditor2
{
    /// <summary>
    /// Interaktionslogik für MapObjectView.xaml
    /// </summary>
    public partial class MapObjectView : Page
    {
        private const int PageLimit = 10;

        public int PageOffset = 0;

        public int DisplayCount => DisplayRows.Count;
        public int FullCount => FullObjectsRows.Count;

      
      
        public ObservableCollection<ObjectRow> DisplayRows { get; set; }

 
        private List<ObjectRow> FullObjectsRows;

        public MapObjectView()
        {

            DisplayRows = new ObservableCollection<ObjectRow>();
            FullObjectsRows = new List<ObjectRow>();

            InitializeComponent();

       

        }

        public void Clear()
        {
            DisplayRows.Clear();
            FullObjectsRows.Clear();
        }
        public void AddObject(MapObject Obj)
        {
            var row = new ObjectRow(Obj);

            if (DisplayRows.Count < PageLimit)
            {
                DisplayRows.Add(row);
            }

            FullObjectsRows.Add(row);
            
        }

        public void RemoveObject(MapObject Obj)
        {

        }
        public bool NextPage()
        {
            if (FullCount <= PageLimit)
            {
                return false;
            }
            else
            {
                if((FullCount > (PageOffset + PageLimit)))
                {
                   DisplayList(GetObjectsByOffset(PageOffset));
              


                    PageOffset = PageOffset + PageLimit;
                    return true;
                }

                return false;
            }
        }



        public bool BackPage()
        {
            if (PageOffset > PageOffset + FullCount)
            {
                return false;
            }
            else
            {
                if ((PageOffset- PageLimit) > PageLimit)
                {
                    PageOffset = PageOffset - PageLimit;

                    DisplayList(GetObjectsByOffset(PageOffset));



     
                    return true;
                }

                return false;
            }
        }

        private void DisplayList(List<ObjectRow> List)
        {
            DisplayRows.Clear();

            foreach(var m in List)
            {
                DisplayRows.Add(m);
            }
        }

        private List<ObjectRow> GetObjectsByOffset(int offset)
        {
            return FullObjectsRows.GetRange(offset, PageLimit);
        }
    }
}
