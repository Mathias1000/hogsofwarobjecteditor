﻿using HogsOfWarObjectEditor2.Objects;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogsOfWarObjectEditor2
{
    public class ObjectEditor
    {
        private ObservableCollection<MapObject> ListOfAllObjects;


        public ObjectEditor()
        {
            ListOfAllObjects = new ObservableCollection<MapObject>();
        }


        public void WriteFullObjectData(StreamWriter Stream)
        {


            Stream.Write((ushort)ListOfAllObjects.Count);

            for (int i = 0; i < ListOfAllObjects.Count; i++)
            {
                var obj = ListOfAllObjects[i];

                obj.ObjectId = i;//beige id

                obj.Write(Stream);
            }



        }


        public void AddObject(MapObject Object)
        {
            ListOfAllObjects.Add(Object);

        }
        public void ResetFunctions()
        {
            ListOfAllObjects.Clear();
        }
    }
}

