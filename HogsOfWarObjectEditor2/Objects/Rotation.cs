﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogsOfWarObjectEditor2.Objects
{
    public class Rotation
    {
        public byte RotationHorizontale { get => _RotationHorizontale; set => _RotationHorizontale = value; }
        public byte RotationVecticale { get => _RotationVecticale; set => _RotationVecticale = value; }

        private byte _RotationHorizontale;
        private byte _RotationVecticale;


        public bool Read(StreamReader Reader)
        {
      
            if (!Reader.Read(out _RotationHorizontale) || !Reader.Read(out _RotationVecticale))
            {
        
                return false;
            }
            return true;
        }


        public void Write(StreamWriter Writer)
        {
            Writer.Write(RotationHorizontale);
            Writer.Write(RotationVecticale);
        }
    }
}
