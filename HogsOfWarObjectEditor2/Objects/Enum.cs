﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogsOfWarObjectEditor2.Objects
{
    public enum Pigs
    {
        AC_ME,
        LE_ME,
        ME_ME,
        SB_ME,
        SP_ME,
        SN_ME,
        SA_ME,
        CO_ME,
        HV_ME,
        GR_ME,
    }

    public enum Vehicles

    {
        PP1,
        AIRSHIP1,
        CARRY,
        TANK,
        BOAT,
        AM_TANK,
        AMLAUNCH,
    }


}
