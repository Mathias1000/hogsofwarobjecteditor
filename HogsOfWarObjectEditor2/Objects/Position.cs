﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogsOfWarObjectEditor2.Objects
{
    public class Position
    {
        public ushort PosX { get => _PosX; set => _PosX = value; }
        public ushort PosY { get => _PosY; set => _PosY = value; }
        public ushort PosZ { get => _PosZ; set => _PosZ = value; }

        private ushort _PosX;
        private ushort _PosY;
        private ushort _PosZ;
        public bool ReadPosition(StreamReader Reader)
        {
   


            if(!Reader.Read(out _PosX) 
                || !Reader.Read(out _PosY) 
                || !Reader.Read(out _PosZ))
            {
             
                return false;
            }
            return true;
        }

        public void Write(StreamWriter Writer)
        {
            Writer.Write(PosX);
            Writer.Write(PosY);
            Writer.Write(PosZ);
        }

        public override string ToString()
        {
            return "X :"+PosX +"X:" +PosY+ "Z:"+PosZ;
        }
    }
}
