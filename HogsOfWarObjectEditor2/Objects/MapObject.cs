﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace HogsOfWarObjectEditor2.Objects
{
    public class MapObject
    {
        #region Property
        public string ObjectName { get => _ObjectName; set => _ObjectName = value; }

        public string Unk1 { get => _Unk1; set => _Unk1 = value; }

        public Position MapPosition { get => _MapPosition; set => _MapPosition = value; }

        public int ObjectId { get => _ObjectId; set => _ObjectId = value; }

        public Rotation Rotation { get => _Rotation; set => _Rotation = value; }

        public ushort unk2 { get => _unk2; set => _unk2 = value; }

        public ushort PigRankId { get => _PigRankId; set => _PigRankId = value; }

        public Position CollesionXYZ { get => _CollesionXYZ; set => _CollesionXYZ = value; }

        public ushort CollesionSetting { get => _CollesionSetting; set => _CollesionSetting = value; }

        public byte ObjectValue { get => _ObjectValue; set => _ObjectValue = value; }

        public byte ObjectMultiplier { get => _ObjectMultiplier; set => _ObjectMultiplier = value; }

        public byte SpawnSetting { get => _SpawnSetting; set => _SpawnSetting = value; }



        public byte GroupValue { get => _GroupValue; set => _GroupValue = value; }

        public ushort ScriptEvent { get => _ScriptEvent; set => _ScriptEvent = value; }

        public byte ScriptGroup { get => _ScriptGroup; set => _ScriptGroup = value; }

        public byte ScriptParameter0 { get => _ScriptParameter0; set => _ScriptParameter0 = value; }

        public byte ScriptParameter1 { get => _ScriptParameter1; set => _ScriptParameter1 = value; }

        public string AdvanceScriptBytes { get => ByteUtils.BytesToHex(_AdvanceScriptBytes); }

        public Position ScriptLocation { get => _ScriptLocation; set => _ScriptLocation = value; }

        public byte StartSettings { get => _StartSettings; set => _StartSettings = value; }

        public byte PlayerType { get => _PlayerType; set => _PlayerType = value; }

        public byte AddinotialSetting { get => _AddinotialSetting; set => _AddinotialSetting = value; }

        #endregion
        private string _Unk1 = "NULL";
        private string _ObjectName = "NULL";
        private Position _MapPosition = new Position();
        private int _ObjectId = 0;
        private Rotation _Rotation = new Rotation();
        private ushort _unk2;//unused?
        private ushort _PigRankId = 1;
        private Position _CollesionXYZ = new Position();
        private ushort _CollesionSetting;//0x01 can on walk..
        private byte _ObjectValue = 0;
        private byte _ObjectMultiplier = 0;//using for big values health and shelter vehicles
        private byte _SpawnSetting = 0x3f;//Default normal 7f for parcasute
        private byte _GroupValue;
        private ushort _ScriptEvent;
        private byte _ScriptGroup;
        private byte _ScriptParameter0;
        private byte _ScriptParameter1;
        private byte[] _AdvanceScriptBytes;//17
        private Position _ScriptLocation = new Position();
        private byte _StartSettings;
        private byte _PlayerType;//0  Cpu 1 = Player
        private byte _AddinotialSetting;
        private byte[] UnkownStuff;



        public MapObject()
        {

        }

        public void Write(StreamWriter Writer)
        {
            Writer.WriteString(_ObjectName, 16);
            Writer.WriteString(_Unk1, 16);
            _MapPosition.Write(Writer);
            Writer.Write(_ObjectId);
            _Rotation.Write(Writer);
            Writer.Write(_unk2);
            Writer.Write(_PigRankId);
            _CollesionXYZ.Write(Writer);
            Writer.Write(_CollesionSetting);
            Writer.Write(_ObjectValue);
            Writer.Write(_ObjectMultiplier);
            Writer.Write(_SpawnSetting);
            Writer.Write(_GroupValue);

            Writer.Write(_ScriptEvent);
            Writer.Write(_ScriptGroup);
            Writer.Write(_ScriptParameter0);
            Writer.Write(_ScriptParameter1);
            Writer.Write(_AdvanceScriptBytes);
            _ScriptLocation.Write(Writer);

            Writer.Write(StartSettings);
            Writer.Write(_PlayerType);
            Writer.Write(_AddinotialSetting);

            Writer.Write(UnkownStuff);
        }
        public bool Read(StreamReader Reader)
        {

            if (!Reader.ReadString(out _ObjectName, 16))
            {
                MessageBox.Show("Failed To Read ObjectName");
                return false;
            }

            if (!Reader.ReadString(out _Unk1, 16))

            {
                MessageBox.Show("Failed To unk1 ObjectName");
                return false;
            }

           
            if (!_MapPosition.ReadPosition(Reader))
            {
                MessageBox.Show("Failed To read object position");
                return false;
            }

            if (!Reader.Read(out _ObjectId))
            {
                MessageBox.Show("Failed To read object id");
                return false;
            }

            if (!Rotation.Read(Reader))
            {
                MessageBox.Show("Failed to Read Object Rotations");
                return false;
            }

            if (!Reader.Read(out _unk2))
            {
                MessageBox.Show("Failed to Read unk2");
                return false;
            }


            if (!Reader.Read(out _PigRankId))
            {
                MessageBox.Show("Failed to Read PigRrankid");
                return false;
            }

            if (!_CollesionXYZ.ReadPosition(Reader))
            {
                MessageBox.Show("Failed to Read Collesion");
                return false;
            }

            if (!Reader.Read(out _CollesionSetting))
            {
                MessageBox.Show("Failed to Read collesionadvance");
                return false;
            }

            if (!Reader.Read(out _ObjectValue))
            {
                MessageBox.Show("Failed to Read ObjectValue");
                return false;

            }

            if (!Reader.Read(out _ObjectMultiplier))
            {
                MessageBox.Show("Failed to Read multipler");
                return false;
            }


            if (!Reader.Read(out _SpawnSetting))
            {
                MessageBox.Show("Failed to Read spawnnsetting");
                return false;
            }



            if (!Reader.Read(out _GroupValue)) //GroupId
            {
                MessageBox.Show("Failed to Read  GroupValuey");
                return false;
            }

            if (!Reader.Read(out _ScriptEvent))
            {
                MessageBox.Show("Failed to Read  ScriptEvent");
                return false;
            }


            if (!Reader.Read(out _ScriptGroup))
            {
                MessageBox.Show("Failed to Read  ScriptEvent");
                return false;
            }

            if (!Reader.Read(out _ScriptParameter0))
            {
                MessageBox.Show("Failed to Read  ScriptEvent");
                return false;
            }

            if (!Reader.Read(out _ScriptParameter1))
            {
                MessageBox.Show("Failed to Read  ScriptEvent");
                return false;
            }
            
            if (!Reader.ReadBytes(17, out _AdvanceScriptBytes))
            {
                MessageBox.Show("wtf");
                return false;
            }

            if (!_ScriptLocation.ReadPosition(Reader))
            {
                MessageBox.Show("wtf");
                return false;
            }

            if(!Reader.Read(out _StartSettings))
            {
                MessageBox.Show("Failed to read Startsettings");
                return false;
            }

            if(!Reader.Read(out _PlayerType))
            {
                MessageBox.Show("Failed to Reaad PlayerType");
                return false;
            }

            if(!Reader.Read(out _AddinotialSetting))
            {
                MessageBox.Show("Failed to Read Addin Settings");
                return false; 
            }
            return Reader.ReadBytes(3, out UnkownStuff);
        }
        //  public
    }
}
