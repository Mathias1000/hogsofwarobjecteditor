﻿using System.Windows.Controls;
using HogsOfWarObjectEditor2.Objects;

namespace HogsOfWarObjectEditor2
{
    public partial class ObjectRow : UserControl
    {
        public MapObject Object { get; private set; }

        public ObjectRow()
        {
            InitializeComponent();
        
        }
  
        public ObjectRow(MapObject Object)
        {
            
            this.Object = Object;

            InitializeComponent();

        }

        public void SetNames()
        {
            
            ObjectNameBox.Text = "Name";
            MapX.Text = "MapX";
            MapY.Text = "MapY";
            MapZ.Text = "MapZ";
            RotationHorizontale.Text = "RotationHorizontale";
            RotationVecticale.Text = "RotationVecticale";
            PigRankId.Text = "PigRankId";
            CollesionX.Text = "CollesionX";
            CollesionY.Text = "CollesionY";
            CollesionZ.Text = "CollesionZ";
            CollesionFlag.Text = "CollesionFlag";
            ObjectValue.Text = "ObjectValue";

            ObjectMultiplier.Text = "ObjectMultiplier";

            SpawnSetting.Text = "SpawnSetting";

            GroupValue.Text = "GroupValue";
            ScriptEvent.Text = "ScriptEvent";
            ScriptGroup.Text = "ScriptGroup";
            ScriptParameter0.Text = "ScriptParameter0";
            ScriptParameter1.Text = "ScriptParameter0";
            ScriptX.Text = "ScriptX";
            ScriptY.Text = "ScriptY";
            ScriptZ.Text = "ScriptZ";
            StartSettings.Text = "StartSettings";
            PlayerType.Text = "PlayerType";
            AddinotialSetting.Text = "AddinotialSetting";
            //Health.Text = "health ";
        }
    }
}
